import json
from menu.function import ten_last_record 

def demarrage() :
    nom_fichier = 'scores.json'
    try:
    # Tente de charger le contenu du fichier JSON existant
        with open(nom_fichier, 'r') as f:
            data = json.load(f)
            print("Nom du joueur : Score du joueur")
            for key, valeur in ten_last_record(data): 
                print(f"{valeur['nom']} : {valeur['score']}")
    except FileNotFoundError:
    # Si le fichier n'existe pas, print message
        print("Il n'y a pas de score enregister.")
    except json.JSONDecodeError:
    # Si le fichier est vide ou ne peut pas être décodé, print message
        print("Il n'y a pas de score enregister.")
    
    var = input("\nAppuyer sur 1 pour aller au menu des resultats, ou 2 pour aller au menu de parti : ")
    return var