import json
import menu.function as func
import menu.game as ga

def launch(end) :
    while(not end) :
        var = input("Veuillez renseigner un nom : ")
        if not func.nom_correct(var) : 
            end = True    
        else : 
            nom = var
            score = ga.game(end, nom)
            score = {
                "nom" : score[0],
                "score" : float("{0:.2f}".format(score[1])), 
                "nombre_essaye" : score[2]
            }

            nom_fichier = 'scores.json'
            try:
                # Tente de charger le contenu du fichier JSON existant
                with open(nom_fichier, 'r') as f:
                    data = json.load(f)
            except FileNotFoundError:
                # Si le fichier n'existe pas, crée un dictionnaire vide
                data = {}
            except json.JSONDecodeError:
                # Si le fichier est vide ou ne peut pas être décodé, crée un dictionnaire vide
                data = {}

            # Met à jour la structure JSON existante avec les nouvelles données
            data[len(data)] = score

            # Écrit la structure JSON mise à jour dans le fichier
            with open(nom_fichier, 'w') as f:
                json.dump(data, f, indent=4)

        end = True       
