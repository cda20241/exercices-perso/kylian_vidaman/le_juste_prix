import time
from random import randint
from menu.function import comparer_avec_jp, clear

def game(end, nom) : 
    var_win = 100 
    """ randint(0, 201) """
    dern_val = 0
    nbr_essaye = 0
    start_time = time.time()
    while(not end) :
        var = int(input("Entrer une proposition : "))  
        if isinstance(var, int) :
            rep = comparer_avec_jp(var_win, var)
            if rep == 0  : 
                clear()
                end_time = time.time()
                chrono = end_time - start_time 
                nbr_essaye += 1
                print("Vous avez trouver le juste prix !! ")
                score = (nom, chrono, nbr_essaye)
                print("Voici votre score : (" + score[0] + ", {0:.2f}".format(score[1]) + ", " + str(score[2]) + ")")
                end = True
            elif rep == -1 :
                clear()
                print("C'est moins!     Dernière proposition : " + str(dern_val))
                nbr_essaye += 1
                dern_val = var
            elif rep == 1 :
                clear()
                print("C'est plus!      Dernière proposition : " + str(dern_val))
                nbr_essaye += 1
                dern_val = var
            else : raise ValueError
    return score
