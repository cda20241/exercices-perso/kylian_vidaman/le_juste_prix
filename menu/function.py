import os as os
import keyboard

def nom_correct(name : str) :
    if name != "exit" and name != "" :
        for c in name :
            if c == ":" : 
                return False
        return True
    else: return False

def comparer_avec_jp(jp : int, val : int) :
    if val < jp : return 1 # sortir "c'est plus"
    elif val > jp : return -1 # sortir "c'est moins"
    else : return 0 # juste prix trouvé

# Clear le terminal
def clear(): 
    os.system('cls' if os.name == 'nt' else "clear")

def ten_last_record(dictionnaire):
    # Inverse les clés du dictionnaire pour parcourir en arrière
    cles_inversees = list(dictionnaire.keys())[::-1]
    i =0
    for cle in cles_inversees :
        if i < 10 :
            valeur = dictionnaire[cle]
            yield cle, valeur
            i+=1